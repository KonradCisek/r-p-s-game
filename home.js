function rpsGame(yourChoice)
{
    console.log(yourChoice); 
    var humanChoice, botChoice;
    humanChoice = yourChoice.id

    botChoice = numberToChoice(randToRpsInt());  
    console.log('Computer Choice:', botChoice);

    results = decideWinner(humanChoice, botChoice);
    console.log(results);

    message = finalMessage(results);
    console.log(message);  

    rpsFrontEnd(humanChoice, botChoice, message);//ostateczny widok 
    
}

function randToRpsInt()
{
    return Math.floor(Math.random() * 3);
}

function numberToChoice(number)
{
    return ['rock', 'paper', 'scissoers'] [number];
}

function decideWinner(yourChoice, computerChoice)
{
    var rpsDatabase = {
                'rock': {'scissors': 1, 'rock': 0.5, 'paper': 0},
                'paper': {'rock': 1, 'paper': 0.5, 'scissors': 0},
                'scissors': {'paper': 1, 'scissors': 0.5, 'rock': 0},
                 };
    var yourScore = rpsDatabase[yourChoice][computerChoice];
    var computerScore = rpsDatabase[computerChoice][yourChoice];

    return [yourScore, computerScore];
}

function finalMessage([yourScore, computerScore]) 
{
    if (yourScore === 0)
    {
        return {'message': 'You Lost!', 'color': 'red'};
    }
    
    else if (yourScore === 0.5)
    {
        return {'message': 'You Tied!', 'color': 'yellow'};    
    }
    else 
    {
        return {'message': 'You Won!', 'color': 'green'};
    }
}

function rpsFrontEnd(humanImageChoice, botImageChoice, finalMessage)
{
    var imagesDatabase = {
        'rock': document.getElementById('rock').scr,
        'paper': document.getElementById('paper').src,
        'scissors': document.getElementById('scissors').src
    }
    // usuwanie obrazkow
    document.getElementById('rock').remove();
    document.getElementById('paper').remove();
    document.getElementById('scissors').remove();


var humanDiv = document.createElement('div');
var botDiv = document.createElement('div');
var messageDiv = document.createElement('div');

humanDiv.innerHTML = "<img src='" + imagesDatabase[humanImageChoice] + "' height=250 widht=250'>"
messageDiv.innerHTML = "<h1 style='color: " + finalMessage['color'] + "; front-size: 100px; paddidng: 30px; '>" + finalMessage['message'] + "</h1>"
botDiv.innerHTML = "<img src='" + imagesDatabase[botImageChoice] + "' height=250 widht=250'>"

document.getElementById('flex-box-rps-div').appendChild(humanDiv);
document.getElementById('flex-box-rps-div').appendChild(messageDiv);
document.getElementById('flex-box-rps-div').appendChild(botDiv);
}

